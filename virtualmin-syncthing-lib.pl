use strict;
use warnings;
our (%config);
our (%text);

BEGIN {push(@INC, "..");};

use WebminCore;

&init_config();
&foreign_require("virtual-server", "virtual-server-lib.pl");
&foreign_require("useradmin");

# syncthing_is_install(&domain)
# check syncthing instance already installed on virtual server (domain)
# return 1 if installed and 0 if not.
sub syncthing_is_install{
    my ( $d ) = @_;

    my $config_dir  = "$d->{'home'}/.config/syncthing";
    my $config_file = "$config_dir/config.xml";

    return -f $config_file;
}

# syncthing_is_running(&domain)
# check syncthing instance is running on specific port from config file
# return 1 if is running and 0 if not.
sub syncthing_is_running{
    eval "use XML::LibXML;";
    my ( $d ) = @_;
    if(&syncthing_is_install($d)){
        my $config_dir  = "$d->{'home'}/.config/syncthing";
        my $config_file = "$config_dir/config.xml";
        my $dom = XML::LibXML->load_xml( location => $config_file );
        my ($gui) = $dom->findnodes('/configuration/gui');
        my $address = $gui->findnodes('address');
        my @split_port = split(':',$address);
        my $port = $split_port[1];
        my $cmd = "lsof -nP -iTCP:$port -sTCP:LISTEN";
        return &virtual_server::run_as_domain_user($d,$cmd);
    }
}

# syncthing_instance_infos(&domain)
# get syncthing instance information from config file
# return hash object.
sub syncthing_instance_infos {
    eval "use XML::LibXML;";
    my ($d) = @_;
    my %result;

    if (&syncthing_is_install($d)) {
        my $config_dir = "$d->{'home'}/.config/syncthing";
        my $config_file = "$config_dir/config.xml";

        my $dom = XML::LibXML->load_xml(location => $config_file);

        my ($device) = $dom->findnodes('/configuration/device');
        my ($gui) = $dom->findnodes('/configuration/gui');

        # extract instance information's
        my $id = $device->findvalue('./@id');
        my $name = $device->findvalue('./@name');
        my $user = $gui->findnodes('user');
        my $address = $gui->findnodes('address');
        my @port = split(':', $address);
        my $instance_link = "http://$name/syncthing/";

        # prepare instance information
        $result{'instance_id'} = $id;
        $result{'instance_url'} = "<a href=\"$instance_link\" target=\"_blank\">$instance_link</a>";
        $result{'instance_port'} = $port[1];
        $result{'instance_user'} = $user;
        $result{'instance_log_path'} = "$d->{'home'}/logs/syncthing.log";
        $result{'instance_status'} = &syncthing_is_running($d);
    }
    return (\%result);
}

# syncyhing_update_cridential(&domain,$input)
# Update syncthing instance credential username and password
# restart server in order to apply update to the instance.
# you should provide new credential in order to get access.
sub syncyhing_update_cridential {
    eval "use XML::LibXML;";
    my ($d, $opt) = @_;
    my %result;
    my $config_dir = "$d->{'home'}/.config/syncthing";
    my $config_file = "$config_dir/config.xml";

    # print $config_dir;
    my $config_dom = XML::LibXML->load_xml(location => $config_file);
    my ($el_gui) = $config_dom->findnodes('/configuration/gui');

    # setting user
    my ($el_user) = $el_gui->findnodes('user');
    if (!$el_user) {
        $el_user = $config_dom->createElement('user');
        $el_gui->appendChild($el_user);
    }
    $el_user->removeChildNodes();
    $el_user->appendText($opt->{'user'} || $d->{'user'});

    # setting password
    my ($el_password) = $el_gui->findnodes('password');
    if (!$el_password) {
        $el_password = $config_dom->createElement('password');
        $el_gui->appendChild($el_password);
    }
    my $password_hash = substr(`htpasswd -bnBC 10 "" $opt->{'password'}`, 1);
    $password_hash =~ s/^\s+|\s+$//g;
    $el_password->removeChildNodes();
    $el_password->appendText($password_hash);

    # update syncthing xml
    $config_dom->toFile($config_file);

    # restart syncthing server
    &$virtual_server::first_print($text{'prepare_restart_start'});
    &syncthing_stop_instance_server($d, $opt);
    &syncyhing_start_instance_server($d, $opt);
    &$virtual_server::second_print($text{success_update});
}

# syncyhing_start_instance_server(&domain,$input)
# restart syncthing instance server.
sub syncyhing_start_instance_server {
    my ($d, $opts) = @_;
    if (!$opts->{'logfile'}) {
        $opts->{'logfile'} = "$d->{'home'}/logs/syncthing.log";
    }
    &syncthing_start_server($d, $opts);
}

# syncthing_start_server(&domain,$input)
# start syncthing server.
sub syncthing_start_server {
    my ($d, $opts) = @_;
    my $syncthing = &has_command($config{'syncthing_cmd'} || "/usr/bin/syncthing");
    my $cmd = "$syncthing -no-browser -logflags=2 -logfile=$opts->{'logfile'}";
    &virtual_server::run_as_domain_user($d, $cmd, 1);
    &$virtual_server::first_print(&syncthing_is_running($d) ? $text{'restart_server'} : $text{'start_server'});
}

# syncthing_stop_instance_server(&domain,$input)
# stop syncthing instance server.
sub syncthing_stop_instance_server {
    my ($d, $sinfo) = @_;
    my $opts = $sinfo->{'opts'};

    &syncthing_stop_server($d, $opts);
    &foreign_require("init");

    my $name = "syncthing-$d->{'user'}";
    if (defined(&init::delete_at_boot)) {
        &init::delete_at_boot($name);
    }
    else {
        &init::disable_at_boot($name);
    }
}

# syncthing_stop_server(&domain,$input)
# stop syncthing server.
sub syncthing_stop_server {
    eval "use XML::LibXML;";
    my ($d, $opts) = @_;

    my $config_dir = "$d->{'home'}/.config/syncthing";
    my $config_file = "$config_dir/config.xml";
    my $config_dom = XML::LibXML->load_xml(location => $config_file);
    my $gui_url = $config_dom->findnodes('/configuration/gui/address/text()');
    my $cmd = "";

    if ($gui_url =~ m!^(([^:/?#]+)://)?(([^/?#:]*))?(:(\d+))([^?#]*)(\?([^#]*))?(#(.*))?!) {
        my $gui_port = $6;
        $cmd = "fuser -n tcp $gui_port -k -SIGTERM";
    }
    else {
        my $syncthing = &has_command($config{'syncthing_cmd'} || "/usr/bin/syncthing");
        $cmd = "kill `pgrep -u $d->{'user'} -f $syncthing`";
    }
    &virtual_server::run_as_domain_user($d, $cmd);
    &$virtual_server::first_print($text{'stop_server'});
}