#!/usr/bin/perl
use strict;
use warnings;

our (%access, %config, %text, %in);
our $module_name;

require './virtualmin-syncthing-lib.pl';
&ReadParse();

my $d;
if ($in{'dom'}) {
    $d = &virtual_server::get_domain($in{'dom'});
}

# Security check
&error_setup($virtual_server::text{'scripts_ierr'});
&virtual_server::can_edit_domain($d) || &error($virtual_server::text{'edit_ecannot'});
&virtual_server::domain_has_website($d) && $d->{'dir'} || &error($virtual_server::text{'scripts_eweb'});
# Protocol may be conditional
my $proto = "https://";
$proto = "http://"
    if (!&virtual_server::domain_has_ssl($d));
# Make link look nice
my $header_link = &ui_link("$proto$d->{'dom'}/syncthing", &virtual_server::domain_in($d), undef,
    " target=\"blank\"");
# Separate text and domain name
$header_link =~ s/>(?<T>.*?)(<.*?>.*?<\/.*?>)</>$2</;
$header_link = "$+{T}$header_link";

# Page title, must be first UI thing
&ui_print_header($header_link, $text{'index_title'}, "", undef, 1, 1);

#
my $page = "index.cgi?dom=$d->{'dom'}&mode=";

my ($instance_info) = &syncthing_instance_infos($d);
my $is_installed = $instance_info && $instance_info->{'instance_id'};
my $instance_status = $instance_info->{'instance_status'};
print &ui_table_start($text{'syncthing_infos'}, "width=100%", 2);
if (!$is_installed) {
    print &ui_table_row('', $text{'syncthing_notinstalled'}, 2);
}
else {
    my $instance_is_on = $instance_status ? '<strong style="color: green;">On</strong>' : '<strong style=" color: red;">Off</strong>';
    print &ui_table_row('Instance id', $instance_info->{'instance_id'}, 2);
    print &ui_table_row('Instance URL', $instance_info->{'instance_url'}, 2);
    print &ui_table_row('Port', $instance_info->{'instance_port'}, 2);
    print &ui_table_row('User', $instance_info->{'instance_user'}, 2);
    print &ui_table_row('Status', $instance_is_on, 2);
}
print &ui_table_end();
print &ui_hr();

my $other_page = "index.cgi?dom=$d->{'dom'}&mode=";
my @tab = (
    $is_installed ? ([ 'credential', 'User credential', $page . "credential" ]) : (),
    $is_installed ? ([ 'servercontrol', 'Server control', $page . "servercontrol" ]) : (),
);

print &ui_tabs_start(\@tab, "mode", $in{'mode'} ||
    ($is_installed ? 'credential' : 'servercontrol'), 1);

if ($is_installed) {
    # update syncthing user credential
    print &ui_tabs_start_tab('mode', 'credential');
    print "<p>$text{'index_tab_credential'}</p>\n";
    print &ui_form_start('./syncthing-edit-credential.cgi', 'post', undef, ("data=unbuffered-header-processor"));
    print &ui_table_start();
    print &ui_table_row("", "", 2);
    print &ui_hidden('dom', $d->{'id'});
    print &ui_table_row("User", &ui_textbox('user', "$instance_info->{'instance_user'}"), 2);
    print &ui_table_row("Password", &ui_password("password"), 2);
    print &ui_table_row("", &ui_submit('Update', "update", 0), 2);
    print &ui_table_end();
    print &ui_form_end();
    print &ui_tabs_end_tab();

    # syncthing instance control
    print &ui_tabs_start_tab("mode", "servercontrol");
    print "<p>$text{'index_virtual_server_instance_control'}</p>\n";
    my $btn_star = $instance_status ? "start" : "restart";
    my $btn_star_label = ! $instance_status ? $text{'index_start'} : $text{'index_restart'};
    print &ui_table_start();
    print &ui_table_row(
        $text{'index_restart_server'},
        &ui_form_start('./syncthing-restart-server.cgi', 'post', undef, ("data=unbuffered-header-processor"))
            . &ui_hidden('dom', $d->{'id'})
            . &ui_submit($btn_star_label, $btn_star, 0)
            . &ui_form_end(), 2);

    print &ui_table_row(
        $text{'index_stop_server'},
        &ui_form_start('./syncthing-stop-server.cgi', 'post', undef, ("data=unbuffered-header-processor"))
            . &ui_hidden('dom', $d->{'id'})
            . &ui_submit($text{'index_stop'}, "stop", undef, undef, 'fa fa-ban', 'btn-danger')
            . &ui_form_end(), 2);
    print &ui_table_end();
    print &ui_tabs_end_tab();
}